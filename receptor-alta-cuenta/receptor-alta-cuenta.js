import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-pages/iron-pages.js'

/**
 * @customElement
 * @polymer
 */
class ReceptorAltaCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
  <!--    <input type="id" placeholder="id"  value="{{id::input}}"/> -->






  <!--    <h2>La cuenta es [[iban]]y el es[[balance]]</h2> -->
  <!--    <h2>y mi id es  [[id]]</h2>-->

  <iron-ajax

     id="doAltaCuenta"
     url="http://localhost:3000/apitechu/v2/altacuenta"
     handle-as="json"
     content-type="application/json"
     method="POST"
     on-response="showData"
     on-error="showError"
  >
  </iron-ajax>



    `;
  }
  static get properties(){
    return {
      opcionaltacuenta: {
        type: Boolean, observer: "_altacuentaChanged" , value: false
      },
      iban: {
        type: String
      },
      id: {
        type: Number
      },
      balance: {
        type: Number
      }
    }
  };


  _altacuentaChanged(newValue, oldValue){
      console.log("alta cuenta value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
      console.log("la id " + this.id)
  //    if (this.selectormovimientos) {
  //    this.$.receiver1.id=newValue
  //    this.$.receiver1.pagina = 1
  //    this.$.receiver2.id=newValue
    if(newValue){

      var loginData = {
      "iban" : this.iban,
      "id" :  this.id,
      "balance":this.balance

      }
      console.log(loginData)

      this.$.doAltaCuenta.body = JSON.stringify(loginData)
      this.$.doAltaCuenta.generateRequest();

    }
    this.opcionaltacuenta = false


  //    }
    }

  showData(data){
    console.log("showData")
    console.log(data.detail.response)
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "msg" : data.detail.response.msg
          },
          "bubbles": true
        }
      )
    )
  }

  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)

    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "msg" : "hubo un error en el alta"
          },
          "bubbles": true
        }
      )
    )
  }
}



window.customElements.define('receptor-alta-cuenta', ReceptorAltaCuenta);
